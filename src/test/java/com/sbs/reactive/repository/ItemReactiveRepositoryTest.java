package com.sbs.reactive.repository;

import com.sbs.reactive.model.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@DirtiesContext
//@SpringBootTest
@ActiveProfiles("dev")
public class ItemReactiveRepositoryTest {

    @Autowired
    ItemReactiveRepository itemReactiveRepository;

    List<Item> itemList= Arrays.asList(new Item(null,"Samsung TV",40000.0),
            new Item("100","LG TV",30000.0),
            new Item(null,"Apple Watch",50000.0),
            new Item(null,"Beats HeadPhones",1500.0),
            new Item("101","Bose HeadPhones",5500.0));


    @BeforeEach
    public void setUp(){
        itemReactiveRepository.deleteAll()
                .thenMany(Flux.fromIterable(itemList))
                .flatMap(itemReactiveRepository::save)
        .doOnNext(item -> System.out.println("Inserted item is : "+item)).blockLast();
    }

    @Test
    public void getAllItems(){
        StepVerifier.create(itemReactiveRepository.findAll())
        .expectSubscription()
        .expectNextCount(5)
        .verifyComplete();
    }

    @Test
    public void getItemById(){
        StepVerifier.create(itemReactiveRepository.findById("101"))
        .expectSubscription()
        .expectNextMatches(item -> item.getDescription().equals("Bose HeadPhones"))
        .verifyComplete();
    }

    @Test
    public void findItemsByDescription(){
        StepVerifier.create(itemReactiveRepository.findByDescription("Bose HeadPhones").log("findItemsByDescription() :"))
                .expectSubscription().expectNextCount(1).verifyComplete();
    }

    @Test
    public void saveItem(){
        Item item=new Item(null,"OnePlus 8 pro",38000.0);
        Mono<Item> savedItem = itemReactiveRepository.save(item);

        StepVerifier.create(savedItem.log("saved item : "))
                .expectSubscription()
                .expectNextMatches( item1 -> item1.getId()!=null && item1.getDescription().equals("OnePlus 8 pro"))
                .verifyComplete();

    }

    @Test
    public void updateItem(){
        double newPrice=20000.00;
        Mono<Item> updatedItem = itemReactiveRepository.findByDescription("LG TV")
                .map(item -> {
                    item.setPrice(newPrice);
                    return item;
                }).flatMap(item -> itemReactiveRepository.save(item));

        StepVerifier.create(updatedItem.log("updated item : "))
                .expectSubscription()
                .expectNextMatches(item -> item.getPrice()==20000.00)
                .verifyComplete();
    }

    @Test
    public void deleteItemById(){
        Mono<Void> deletedItem = itemReactiveRepository.findById("101")
                .map(Item::getId)
                .flatMap(id -> itemReactiveRepository.deleteById(id));
        StepVerifier.create(deletedItem.log("Deleted item : "))
                .expectSubscription()
                .verifyComplete();

        StepVerifier.create(itemReactiveRepository.findAll().log("new item list after delete : "))
                .expectSubscription()
                .expectNextCount(4).verifyComplete();
    }

    @Test
    public void deleteItem(){
        Mono<Void> deletedItem = itemReactiveRepository.findByDescription("LG TV")
                .flatMap(item -> itemReactiveRepository.delete(item));
        StepVerifier.create(deletedItem.log("Deleted item : "))
                .expectSubscription()
                .verifyComplete();

        StepVerifier.create(itemReactiveRepository.findAll().log("new item list after delete : "))
                .expectSubscription()
                .expectNextCount(4).verifyComplete();
    }
}
