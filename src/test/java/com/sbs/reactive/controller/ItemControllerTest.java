package com.sbs.reactive.controller;

import com.sbs.reactive.model.Item;
import com.sbs.reactive.repository.ItemReactiveRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext
@AutoConfigureWebTestClient
@ActiveProfiles("dev")
public class ItemControllerTest {

    @Autowired
    WebTestClient webTestClient;
    @Autowired
    ItemReactiveRepository itemReactiveRepository;

    public List<Item> data(){
        return Arrays.asList(new Item("100","LG TV",30000.0),
                new Item("101","Samsung TV",40000.0),
                new Item("102","Apple Watch",50000.0));
    }

    @BeforeEach
    public void setUp(){
        itemReactiveRepository.deleteAll()
                .thenMany(Flux.fromIterable(data()))
                .flatMap(itemReactiveRepository::save)
                .doOnNext(item -> System.out.println("Inserted item is : "+item)).blockLast();
    }

    @Test
    public void getAllItems(){
        webTestClient.get().uri("/items")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class).hasSize(3);
    }

    @Test
    public void getAllItems_2(){
        webTestClient.get().uri("/items")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class).hasSize(3)
        .consumeWith(response ->{
            List<Item> items = response.getResponseBody();
            assert items != null;
            items.forEach(item -> Assertions.assertNotNull(item.getId()));
        });
    }

    @Test
    public void getAllItems_3(){
        Flux<Item> itemsFlux = webTestClient.get().uri("/items")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(Item.class)
                .getResponseBody();

        StepVerifier.create(itemsFlux.log("values : "))
                .expectNextCount(3).verifyComplete();
    }

    @Test
    public void getItemById(){
        webTestClient.get().uri("/item/{id}","100")
                .exchange().expectStatus().isOk()
                .expectBody()
                .jsonPath("$.price",30000.0);
    }

    @Test
    public void getItemById_NotFound(){
        webTestClient.get().uri("/item/{id}","1001")
                .exchange().expectStatus().isNotFound();
    }

    @Test
    public void saveItem(){
        Item item=new Item(null,"IPhone X",100000.65);
        webTestClient.post().uri("/save")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(item),Item.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.description").isEqualTo("IPhone X")
                .jsonPath("$.price").isEqualTo("100000.65");
    }

    @Test
    public void deleteItem(){
        webTestClient.delete().uri("/delete/{id}","100")
                .accept(MediaType.APPLICATION_JSON).exchange()
                .expectStatus().isOk()
        .expectBody(Void.class);

    }

    @Test
    public void updateItem(){
        double newPrice=45000.45;
        Item item=new Item("101","Samsung TV",newPrice);
        webTestClient.put().uri("/update/item/{id}", 101)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(item),Item.class)
                .exchange()
                .expectStatus().isOk()
        .expectBody().jsonPath("$.price",newPrice);
    }

    @Test
    public void updateItem_NotFound(){
        double newPrice=45000.45;
        Item item=new Item("101","Samsung TV",newPrice);
        webTestClient.put().uri("/update/item/{id}", 1011)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(item),Item.class)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void runTimeException(){
        webTestClient.get().uri("/get/exception")
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody(String.class)
                .isEqualTo("Exception Occurred");
    }
}
