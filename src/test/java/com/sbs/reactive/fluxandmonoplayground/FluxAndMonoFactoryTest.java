package com.sbs.reactive.fluxandmonoplayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class FluxAndMonoFactoryTest {

    List<String> name= Arrays.asList("adam","anna","jack","jenny");

    @Test
    public void fluxUsingIterable(){
        Flux<String>  namesFlux= Flux.fromIterable(name).log() ;

        StepVerifier.create(namesFlux)
                .expectNext("adam","anna","jack","jenny").verifyComplete();
    }

    @Test
    public void fluxUsingArray(){
        String[] names=new String[]{"adam","anna","jack","jenny","rishabh"};
        Flux<String> stringFlux = Flux.fromArray(names).log();

        StepVerifier.create(stringFlux)
                .expectNext("adam","anna","jack","jenny","rishabh").verifyComplete();

    }

    @Test
    public void monoUsingJustOrEmpty(){
        Mono<Object> objectMono = Mono.justOrEmpty(null);
        StepVerifier.create(objectMono.log())
        .verifyComplete();
    }

    @Test
    public void monoUsingSupplier(){
        Supplier<String> mono=()->"rishabh";

        Mono<String> stringMono = Mono.fromSupplier(mono);
        System.out.println(mono.get());
        StepVerifier.create(stringMono.log()).expectNext("rishabh").verifyComplete();
    }
}
