package com.sbs.reactive.fluxandmonoplayground;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class FluxMonoBackPressureTest {

    @Test
    public void backPressureTest() {
        Flux<Integer> rangeFlux = Flux.range(1, 10);
        StepVerifier.create(rangeFlux.log())
                .expectSubscription()
                .thenRequest(2)
                .expectNext(1, 2).thenRequest(1).expectNext(3).thenCancel()
                .verify();
    }

    @Test
    public void backPressure() {
        Flux<Integer> rangeFlux = Flux.range(1, 10).log();
        rangeFlux.subscribe(element -> System.out.println("Element is : " + element)
                , (e) -> System.err.println("Exception is : " + e)
                , () -> System.out.println("Done")
                , (subscription -> subscription.request(2)));
    }

    @Test
    public void customized_backPressure(){
        Flux<Integer> rangeFlux = Flux.range(1, 10).log();

        rangeFlux.subscribe(new BaseSubscriber<Integer>() {
            @Override
            protected void hookOnNext(Integer value) {
                request(1);
                System.out.println("value is : "+value);
                if(value==4)
                    cancel();
            }
        });
    }

    @Test
    public void coldPublisherTest() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A","B","C","D","E","F").delayElements(Duration.ofSeconds(1));
        stringFlux.subscribe(s-> System.out.println("subscribe1 : "+s));
        Thread.sleep(2000);
        stringFlux.subscribe(s-> System.out.println("subscribe2 : "+s));
        Thread.sleep(4000);


    }
}
