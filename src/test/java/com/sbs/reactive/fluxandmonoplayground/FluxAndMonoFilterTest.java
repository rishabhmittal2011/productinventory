package com.sbs.reactive.fluxandmonoplayground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxAndMonoFilterTest {

    @Test
    public void fluxUsingArray(){
        String[] names=new String[]{"adam","anna","jack","jenny","rishabh"};
        Flux<String> stringFlux = Flux.fromArray(names)
                .filter(name -> name.startsWith("a"))
                .log();
        StepVerifier.create(stringFlux)
                .expectNext("adam","anna").verifyComplete();

    }
}
