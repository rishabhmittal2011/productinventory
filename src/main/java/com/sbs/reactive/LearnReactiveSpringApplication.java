package com.sbs.reactive;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class LearnReactiveSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnReactiveSpringApplication.class, args);
		log.info("Application is running");
	}

}
