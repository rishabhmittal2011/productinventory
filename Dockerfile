FROM openjdk:8
ADD build/libs/*.jar product-inventory.jar
EXPOSE 8082
ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","product-inventory.jar"]
